/* Copyright © 2014 Ryan Porterfield <http://ryanporterfield.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** *********************************************************************** **
 ** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Implementation for the EntityManager class                  **
 **                                                                         **
 ** @author     Ryan Porterfield                                            **
 ** @license    GNU Lesser General Public License (LGPL) v3                 **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/

#include "entity.hpp"

#ifdef WIN32
#include <Rpc.h>
#else
#include <uuid/uuid.h>
#endif // WIN32

#include <algorithm>
#include <ciso646>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

using namespace shadowengine;
using namespace entity;


/*  *********************************************************************** **
 **                             Component Class                             **
 ** *********************************************************************** **/

/**
 *  @brief      Create a new Component.
 */
Component::Component(const uint32_t component_id)
        : component_id(component_id) {
}


/**
 *  @brief      Destroy a Component.
 */
Component::~Component() {
}

/*  *********************************************************************** **
 **                           EntityManager Class                           **
 ** *********************************************************************** **/

/**
 *  @brief      Create a new, empty EntityManager.
 */
EntityManager::EntityManager() {
}


/**
 *  @brief      Destroy an EntityManager.
 */
EntityManager::~EntityManager() {
}


/**
 *  @brief      Add a Component to an entity managed by this EntityManager.
 *
 *  @param uuid The string UUID representation of the entity.
 *  @param c    A unique_ptr to a Component being mapped to the
 *              entity.
 *  @return     @c true if the component was added, otherwise @c false.
 */
bool
EntityManager::AddComponent(const std::string uuid,
                            std::unique_ptr<Component> c) {
    std::pair<ECMap::iterator, bool> ret;
    ECMap* components; 
    try {
        components = component_map_.at(c->component_id).get();
    } catch (const std::out_of_range& e) {
        components = CreateECMap(c->component_id);
    }
    ret = components->insert(std::make_pair(uuid, std::move(c)));
    return ret.second;
}


/**
 *  @brief      Create a new unique_ptr to a new ECMap.
 *
 *  @param id   The component_id that needs an ECMap.
 *  @return     A pointer to the new ECMap.
 */
ECMap*
EntityManager::CreateECMap(const uint32_t id) {
    std::pair<ComponentMap::iterator, bool> status;
    // Make a unique_ptr to a new ECMap
    std::unique_ptr<ECMap> ptr = std::make_unique<ECMap>();
    // Make a pair from the component_id and the unique_ptr
    auto ec_map = std::make_pair(id, std::move(ptr));
    // Insert the pair into component_map_
    status = component_map_.insert(std::move(ec_map));
    std::pair<const uint32_t, std::unique_ptr<ECMap> >& iter = *(status.first);
    return iter.second.get();
}


/**
 *  @brief      Create a new entity.
 *
 *  @details    Creates a UUID from either native Windows libaries or native
 *              Linux libraries, then parses the UUID to a string.
 *  @return     A string representation of the generated UUID.
 */
std::string
EntityManager::CreateEntity()
{
#ifdef WIN32
    // Create a UUID using native Windows code
    UUID uuid;
    UuidCreate(&uuid);

    unsigned char* s;
    UuidToStringA(&uuid, &s);
    RpcStringFreeA(&s);
#else
    // Create a UUID using native *nix code
    uuid_t uuid;
    uuid_generate_random(uuid);
    char s[37];
    uuid_unparse(uuid, s);
#endif
    std::string s_uuid = s;
    entities_.push_back(s_uuid);
    return s;
}


/**
 *  @brief      Enable an entity.
 *
 *  @details    If an entity is disabled, entity systems/processors shouldn't
 *              update/listen to it.
 *  @param uuid The UUID being enabled.
 */
void
EntityManager::Enable(const std::string uuid) {
    disabled_.erase(uuid);
}


/**
 *  @brief      Disable an entity.
 *
 *  @details    If an entity is disabled, entity systems/processors shouldn't
 *              update/listen to it.
 *  @param uuid The UUID being disabled.
 */
void
EntityManager::Disable(const std::string uuid) {
    disabled_.insert(uuid);
}


/**
 *  @brief      Get all the Components mapped to an entity.
 *
 *  @details    Slow implementation. Do not use this method unless you
 *              absolutely have to.
 *  @param uuid The UUID of the entity's Components we want.
 *  @return     A vector of pointers to the Components mapped to uuid.
 */
std::vector<Component*>
EntityManager::GetAllComponents(const std::string uuid) const {
    std::vector<Component*> components;
    ComponentMap::const_iterator iter = component_map_.begin();
    for (; component_map_.end() not_eq iter; ++iter) {
        try {
            components.push_back(iter->second->at(uuid).get());
        } catch(const std::out_of_range& e) {}
    }
    return components;
}


/**
 *  @brief      Get a Component mapped to an entity.
 *
 *  @param uuid The UUID of the entity's Component we want.
 *  @param id   The component_id of the type of Component we want.
 *  @return     A pointer to a Component of type id or NULL if the entity
 *              doesn't have one.
 */
Component*
EntityManager::GetComponent(const std::string uuid,
                            const uint32_t id) const {
    Component* c = NULL;
    try {
        const ECMap* components = component_map_.at(id).get();
        c = components->at(uuid).get();
    } catch (const std::out_of_range& e) {}
    return c;
}


/**
 *  @brief      Get a list of all entity UUIDs.
 *
 *  @details    Returns a const std::vector<std::string>* to the canonical
 *              vector of entities. This function DOES NOT create a copy of the
 *              vector, so the pointer returned from this function should not
 *              be used while creating or killing entities.
 *  @return     A pointer to the canonical vector of UUIDs.
 */
const std::vector<std::string>*
EntityManager::GetEntities() const {
    return &entities_;
}


/**
 *  @brief      Checks if an entity has a Component of type id mapped to it.
 *
 *  @param uuid The UUID of the entity's Components being checked.
 *  @param id   The component_id of the type of Component we want.
 *  @return     @c true if the entity has a Component of type id, otherwise
 *              @c false.
 */
bool
EntityManager::HasComponent(const std::string uuid,
                            const uint32_t id) const {
    try {
        const ECMap* components = component_map_.at(id).get();
        components->at(uuid);
        return true;
    } catch (const std::out_of_range& e) {
        return false;
    }
}


/**
 *  @brief      Check if the specified entity is enabled.
 *
 *  @param uuid The UUID of the entity being checked.
 *  @return     @c true if enabled or @c false if it is disabled.
 */
bool
EntityManager::IsEnabled(const std::string uuid) const {
    return disabled_.end() == disabled_.find(uuid);
}


/**
 *  @brief      Removes an entity and destroys all its Components.
 *
 *  @param uuid The UUID of the entity being killed.
 */
void
EntityManager::KillEntity(const std::string uuid) {
    RemoveAllComponents(uuid);
    std::vector<std::string>::iterator pos;
    pos = std::find(entities_.begin(), entities_.end(), uuid);
    entities_.erase(pos);
}


/**
 *  @brief      Remove all Components mapped to entity uuid.
 *
 *  @param uuid The UUID of the entity's Components being removed.
 */
void
EntityManager::RemoveAllComponents(const std::string uuid) {
    ComponentMap::iterator iter = component_map_.begin();
    for (; component_map_.end() not_eq iter; ++iter)
        iter->second->erase(uuid);
}


/**
 *  @brief      Remove a component from an entity.
 *
 *  @param uuid The UUID of the entity's Components being removed.
 *  @param id   The component_id of the type of Component being removed.
 */
void
EntityManager::RemoveComponent(const std::string uuid,
                               const uint32_t component_id) {
    try {
        ECMap* components = component_map_.at(component_id).get();
        components->erase(uuid);
    } catch (const std::out_of_range& e) {}
}


/*  *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
