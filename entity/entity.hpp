/*
 * Copyright © 2014 Ryan Porterfield <http://ryanporterfield.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Entity manager/processor to process entities and components **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @version    0.0.1:20141228                                              **
 ** @license    GNU Lesser General Public License (LGPL) v3                 **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/

#ifndef SHADOW_ENTITY_H_
#define SHADOW_ENTITY_H_

#ifdef WIN32
#include <Rpc.h>
#else
#include <uuid/uuid.h>
#endif // WIN32

#include <stdint.h>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>


/** @namespace  shadowengine
 *  @brief      The namespace containing the public API for the ShadowEngine.
 */
namespace shadowengine {

/** @namespace  entity
 *  @brief      The namespace containing the public API for the
 *              Entity-Component manager for the ShadowEngine.
 */
namespace entity {


/** @class      Component entity.hpp "src/entity.hpp"
 *  @brief      Base Component class.
 *
 *  @details    The base, top-level component. The class holds only a
 *              component_id which should be unique to each subclass, not each
 *              instance of Component, and is only used as a super class for
 *              concrete components. IE.
 *              @code class ComponentName : public Component {
 *                  ComponentName (std::string name) : Component (0) {}
 *                  ...
 *              } @endcode
 */
class Component {
  public:
    Component               (const uint32_t component_id);
    virtual ~Component      ();
    const uint32_t          component_id;
};


/** @typedef ECMap
 *  @brief      The unordered_map that maps entity UUIDs to a Component.
 */
typedef std::unordered_map< std::string, std::unique_ptr<Component> > ECMap;

/** @typedef ComponentMap
 *  @brief      The unordered_map that maps Component IDs to an ECMap.
 */
typedef std::unordered_map< uint32_t, std::unique_ptr<ECMap> > ComponentMap;


/** @class       EntityManager entity.hpp "src/entity.hpp"
 *  @brief       Manages entity and Component mappings.
 *
 *  @details    Handles the creation and deletion of entity UUIDS as well as
 *              handling the mapping of entities to Components.
 */
class EntityManager {
  public:
    EntityManager();

    EntityManager(const EntityManager& copy_from_me) = delete;

    virtual ~EntityManager();

    virtual bool AddComponent(const std::string uuid,
                       std::unique_ptr<Component> component);

    virtual std::string CreateEntity();

    virtual void Enable(const std::string uuid);

    virtual void Disable(const std::string uuid);

    virtual std::vector<Component*>
            GetAllComponents(const std::string uuid) const;

    virtual Component* GetComponent(const std::string uuid,
                                    const uint32_t id) const;

    virtual const std::vector<std::string>* GetEntities() const;

    virtual bool HasComponent(const std::string uuid,
                              const uint32_t id) const;

    virtual bool IsEnabled(const std::string uuid) const;

    virtual void KillEntity(const std::string uuid);

    virtual void RemoveAllComponents(const std::string uuid);

    virtual void RemoveComponent(const std::string uuid, const uint32_t id);

  private:
    virtual ECMap* CreateECMap(const uint32_t id);

    /**
     *  @brief  Map of all entities and their components.
     * 
     * Top level map stores component types and a vector of all
     * components of that type. The vector maps components to
     * the index of the entity they are registered to.
     */
    ComponentMap component_map_;
    /**
     * A vector of each entity and whether or not it is disabled.
     */
    std::unordered_set<std::string> disabled_;
    /**
     * Vector of all entities registered the the manager.
     */
    std::vector<std::string> entities_;
};


} // namepsace entity
} // namespace shadowengine
#endif // SHADOW_ENTITY_H_


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
