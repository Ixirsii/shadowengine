/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Formatter class for messages logged to the console/terminal **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <simpleformatter.h>
#include <string>
#include <time.h>
#include <logrecord.h>

using namespace Logging;


/**
 * @brief       Create a new SimpleFormatter
 */
SimpleFormatter::SimpleFormatter ()
{}

/**
 * @brief       Destroy a SimpleFormatter
 */
SimpleFormatter::~SimpleFormatter ()
{}

/**
 * @brief       Formats a string based on the log level.
 *
 * @details     Generate a string with the messages log level.
 * @param level The log level of the log.
 * @return      Log level as a string
 */
std::string
SimpleFormatter::get_level_str (Level level)
{
    std::string level_str = "[";
    switch (level) {
        case logSEVERE:
            level_str += "ERROR"; break;
        case logWARNING:    
            level_str += " WARN"; break;
        case logINFO:       
            level_str += " INFO"; break;
        case logDEBUG1:     
        case logDEBUG2:     
        case logDEBUG3:     
        case logDEBUG4:     
            level_str += "DEBUG"; break;
        default:
            level_str += "     ";
    };
    return level_str + "] ";
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
