/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Formatter class for messages logged to the console/terminal **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#include <baseconsoleformatter.h>
#include <string>
#include <time.h>
#include <logrecord.h>

using namespace Logging;


/**
 * Initialize a new BaseConsoleFormatter
 */
BaseConsoleFormatter::BaseConsoleFormatter ()
{}

/**
 * Un-initialize a BaseConsoleFormatter
 */
BaseConsoleFormatter::~BaseConsoleFormatter ()
{}

/**
 * @brief       Format the log into a printable/readable string.
 *
 * @details     Takes the log level, log time, and message and formats it into
 *              a human readable string that can be printed to a
 *              console/terminal.
 * @param record The record that contains the message being logged.
 * @return      The logged message as a string.
 */
std::string
BaseConsoleFormatter::format (LogRecord& record)
{
    std::string log = "[" + Formatter::get_time_str (record.get_time ()) + "]";
    log += get_level_str (record.get_level ());
    log += record.get_message ();
    return log;
}

/**
 * @brief       Format the log into a printable/readable string.
 *
 * @details     Takes only the message and formats it into a human readable
 *              string that can be printed to a console/terminal.
 * @param record The record that contains the message being logged.
 * @return      The logged message as a string.
 */
std::string
BaseConsoleFormatter::format_message (LogRecord& record)
{
    return record.get_message ();
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
