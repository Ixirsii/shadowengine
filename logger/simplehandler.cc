/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Interface that defines behavior for SimpleHandlers         **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <simplehandler.h>

#include <iostream>

#include <iso646.h>

#include <simpleformatter.h>
#include <level.h>
#include <logrecord.h>

using namespace Logging;


SimpleHandler::SimpleHandler ()
    : Handler (new SimpleFormatter())
{}

SimpleHandler::SimpleHandler (Level level)
    : Handler (new SimpleFormatter(), level)
{}

SimpleHandler::~SimpleHandler ()
{}

void
SimpleHandler::publish (LogRecord& record)
{
    if (not is_loggable (record))
        return;
    std::cout << format_->format (record) << std::endl;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/

