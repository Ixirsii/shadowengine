/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Formatter class for messages logged to the console/terminal **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#include <htmlformatter.h>

#include <iostream>
#include <string>

#include <level.h>
#include <logrecord.h>

using namespace Logging;


HTMLFormatter::HTMLFormatter (std::string logger_name)
    : Formatter (logger_name)
{}

HTMLFormatter::~HTMLFormatter ()
{}

std::string
HTMLFormatter::format (LogRecord& record)
{
    std::string format = format_time (record.get_time ());
    format += format_level (record.get_level ());
    format += format_log_message (record.get_message ());
    return format;
}

std::string
HTMLFormatter::format_level (Level level)
{
    std::string level_str = "<tr>";
    switch (level) {
        case logSEVERE:
            level_str += "<font color=\"800000\">Severe</font>"; break;
        case logWARNING:
            level_str += "<font color=\"808000\">Warning</font>"; break;
        case logDEBUG1:
            level_str += "<font color=\"000080\">Debug 1</font>"; break;
        case logDEBUG2:
            level_str += "<font color=\"8080A0\">Debug 2</font>"; break;
        case logDEBUG3:
            level_str += "<font color=\"1569C7\">Debug 3</font>"; break;
        case logDEBUG4:
            level_str += "<font color=\"3090C7\">Debug 4</font>"; break;
        case logINFO:
        default:
            level_str += "<font color=\"008000\">INFO</font>";
    }
    level_str = level_str + "</tr>";
    return level_str;
}

std::string
HTMLFormatter::format_log_message (std::string message)
{
    return "<tr>" + message + "</tr>\n\t\t\t</td>";
}

std::string
HTMLFormatter::format_message (LogRecord& record)
{
    return "\n\t\t\t<td>\n\t\t\t\t<tr>" + record.get_message () + "</tr>";
}

std::string
HTMLFormatter::format_time (time_t time)
{
    std::string time_str = "\n\t\t\t<td>\n\t\t\t\t<tr>";
    time_str += get_time_str (time);
    time_str += "</tr>";
    return time_str;
}

std::string
HTMLFormatter::get_head ()
{
    return "<DOCTYPE! html>\n<html>\n\t<head>\n\t\t<title>"
        "HTML Log</title>\n\t</head>\n\t<body>\n\t\t<table border=\"0\">"
        "\n\t\t\t<tr>\n\t\t\t\t<strong><td>Time</td><td>Log Level</td>"
        "<td>Message</td></strong>\n\t\t\t</tr>";
}

std::string
HTMLFormatter::get_tail ()
{
    std::string tail = "\t</body>\n</html>";
    return tail;
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
