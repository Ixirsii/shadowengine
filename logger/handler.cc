/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Interface that defines behavior for ConsoleHandlers         **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#include <handler.h>

#include <formatter.h>
#include <level.h>

using namespace Logging;

Handler::Handler (Formatter *format)
    : format_ (format)
{}

Handler::Handler (Formatter *format, Level level)
    : format_ (format), log_level_ (level)
{}

Handler::~Handler ()
{
    delete format_;
}

Formatter *
Handler::get_formatter ()
{
    return this->format_;
}

Level
Handler::get_level ()
{
    return this->log_level_;
}

bool
Handler::is_loggable (LogRecord& record)
{
    return this->log_level_ < record.get_level ();
}

void
Handler::set_formatter (Formatter *f)
{
    this->format_ = f;
}

void
Handler::set_level (Level l)
{
    this->log_level_ = l;
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
