/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Interface that defines behavior for ConsoleHandlers         **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <filehandler.h>

#include <fstream>
#include <string>

#include <iso646.h>

#include <htmlformatter.h>
#include <level.h>
#include <logrecord.h>

using namespace Logging;


/** *********************************************************************** **
 **                          Concrete Constructors                          **
 ** *********************************************************************** **/

/** TODO: Change HTMLFormatter to FileFormatter
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 */
FileHandler::FileHandler (std::string logger, std::string file_name)
    : Handler (new HTMLFormatter(logger)),
    ofile_ (new std::ofstream (file_name))
{
    *ofile_ << format_->get_head ();
}

/** TODO: Change HTMLFormatter to FileFormatter
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 */
FileHandler::FileHandler (std::string logger, std::ofstream *ofile)
    : Handler (new HTMLFormatter(logger)), ofile_ (ofile)
{
    *ofile_ << format_->get_head ();
}

/** TODO: Change HTMLFormatter to FileFormatter
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 * @param level     The maximum @c Level of logs that will be written.
 */
FileHandler::FileHandler (std::string logger, std::string file_name,
        Level level)
    : Handler (new HTMLFormatter(logger), level),
    ofile_ (new std::ofstream (file_name))
{
    *ofile_ << format_->get_head ();
}

/** TODO: Change HTMLFormatter to FileFormatter
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 * @param level     The maximum @c Level of logs that will be written.
 */
FileHandler::FileHandler (std::string logger, std::ofstream *ofile,
        Level level)
    : Handler (new HTMLFormatter(logger), level), ofile_ (ofile)
{
    *ofile_ << format_->get_head ();
}

/** *********************************************************************** **
 **                          Abstract Constructors                          **
 ** *********************************************************************** **/

/**
 * @brief           Construct new FileHandler
 *
 * @param format    A pointer to the @c Formatter that will format LogRecords
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 */
FileHandler::FileHandler (Formatter *format, std::string file_name)
    : Handler (format), ofile_ (new std::ofstream (file_name))
{
    *ofile_ << format_->get_head ();
}

/**
 * @brief           Construct new FileHandler
 *
 * @param format    A pointer to the @c Formatter that will format LogRecords
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 */
FileHandler::FileHandler (Formatter *format, std::ofstream *ofile)
    : Handler (format), ofile_ (ofile)
{
    *ofile_ << format_->get_head ();
}

/**
 * @brief           Construct new FileHandler
 *
 * @param format    A pointer to the @c Formatter that will format LogRecords
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 * @param level     The maximum @c Level of logs that will be written.
 */
FileHandler::FileHandler (Formatter *format, std::string file_name,
        Level level)
    : Handler (format, level), ofile_ (new std::ofstream (file_name))
{
    *ofile_ << format_->get_head ();
}

/**
 * @brief           Construct new FileHandler
 *
 * @param format    A pointer to the @c Formatter that will format LogRecords
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 * @param level     The maximum @c Level of logs that will be written.
 */
FileHandler::FileHandler (Formatter *format, std::ofstream *ofile, Level level)
    : Handler (format, level), ofile_ (ofile)
{
    *ofile_ << format_->get_head ();
}

FileHandler::~FileHandler ()
{
    *ofile_ << format_->get_tail ();
    ofile_->close ();
    delete ofile_;
}

void
FileHandler::publish (LogRecord& record)
{
    if (not is_loggable (record))
        return;
    *ofile_ << format_->format (record) << std::endl;

}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
