/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_HANDLER_H
#define LOGGING_HANDLER_H

#include <formatter.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       Handler
 * @brief       Base class provides common functionality for all handlers.
 *
 * @details     Abstract class that provides limited functionality and defines
 *              general behavior for all handlers.
 */
class Handler
{
public:
    Handler                                 (Formatter *format);
    Handler                                 (Formatter *format, Level level);
    virtual ~Handler                        ();
    Formatter *         get_formatter       ();
    Level               get_level           ();
    bool                is_loggable         (LogRecord& record);
    virtual void        publish             (LogRecord& record) = 0;
    void                set_formatter       (Formatter *format);
    void                set_level           (Level l);
protected:
    Formatter                               *format_;
    Level                                   log_level_;
};

}

#endif // LOGGING_HANDLER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
