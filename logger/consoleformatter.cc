/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Formatter class for messages logged to the console/terminal **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <consoleformatter.h>
#include <string>
#include <time.h>
#include <logrecord.h>

#define DEBUG_COLOR     "\033[01;35m"
#define DEFAULT_COLOR   "\033[00m"
#define INFO_COLOR      "\033[01;32m"
#define SEVERE_COLOR    "\033[01;31m"
#define WARN_COLOR      "\033[01;33m"

using namespace Logging;


/**
 * @brief       Create a new ConsoleFormatter
 */
ConsoleFormatter::ConsoleFormatter ()
{}

/**
 * @brief       Destroy a ConsoleFormatter
 */
ConsoleFormatter::~ConsoleFormatter ()
{}

/**
 * @brief       Formats a string based on the log level.
 *
 * @details     Generate a full colored string with the messages log level.
 *              Requires a 16-color enabled terminal.
 * @param level The log level of the log.
 * @return      Log level as a string
 */
std::string
ConsoleFormatter::get_level_str (Level level)
{
    std::string level_str = "[";
    switch (level) {
        case logSEVERE:
            level_str += SEVERE_COLOR "ERROR"; break;
        case logWARNING:    
            level_str += WARN_COLOR " WARN"; break;
        case logINFO:       
            level_str += INFO_COLOR " INFO"; break;
        case logDEBUG1:     
        case logDEBUG2:     
        case logDEBUG3:     
        case logDEBUG4:     
            level_str += DEBUG_COLOR "DEBUG"; break;
        default:
            level_str += "     ";
    };
    return level_str + DEFAULT_COLOR "] ";
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
