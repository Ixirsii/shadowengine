/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_CONSOLE_FORMATTER_H
#define LOGGING_CONSOLE_FORMATTER_H

#include <string>
#include <baseconsoleformatter.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       SimpleFormatter
 * @brief       Formats messages for output to a terminal.
 */
class SimpleFormatter : public BaseConsoleFormatter
{
public:
    SimpleFormatter                         ();
    virtual ~SimpleFormatter                ();

private:
    std::string         get_level_str       (Level level);
};

}

#endif // LOGGING_CONSOLE_FORMATTER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
