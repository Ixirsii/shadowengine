/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file       htmlformatter.h                                             **  
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @copyright  BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/

#ifndef LOGGING_HTML_FORMATTER_H
#define LOGGING_HTML_FORMATTER_H

#include <string>

#include <formatter.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       HTMLFormatter
 * @brief       Formats logged messages to be written to an html file.
 */
class HTMLFormatter : public Formatter
{
public:
    HTMLFormatter                           (std::string logger_name);
    virtual ~HTMLFormatter                  ();

    virtual std::string format              (LogRecord& record);
    virtual std::string format_message      (LogRecord& record);
    virtual std::string get_head            ();
    virtual std::string get_tail            ();

private:
    std::string         format_level        (Level level);
    std::string         format_log_message  (std::string message);
    std::string         format_time         (time_t time);
};

}

#endif // LOGGING_HTML_FORMATTER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
