/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_SIMPLE_HANDLER_H
#define LOGGING_SIMPLE_HANDLER_H

#include <formatter.h>
#include <handler.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       SimpleHandler
 * @brief       Basic handler that logs messages to the console.
 */
class SimpleHandler : public Handler
{
public:
    SimpleHandler                           ();
    SimpleHandler                           (Level level);
    virtual ~SimpleHandler                  ();

    virtual void        publish             (LogRecord& record);
};

}

#endif // LOGGING_SIMPLE_HANDLER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
