/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Interface that defines behavior for ConsoleHandlers         **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <consolehandler.h>

#include <iostream>

#include <iso646.h>

#include <consoleformatter.h>
#include <level.h>
#include <logrecord.h>

using namespace Logging;


ConsoleHandler::ConsoleHandler ()
    : Handler (new ConsoleFormatter())
{}

ConsoleHandler::ConsoleHandler (Level level)
    : Handler (new ConsoleFormatter(), level)
{}

ConsoleHandler::~ConsoleHandler ()
{}

void
ConsoleHandler::publish (LogRecord& record)
{
    if (not is_loggable (record))
        return;
    std::cout << format_->format (record) << std::endl;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
