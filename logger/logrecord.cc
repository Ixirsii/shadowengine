/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Logging implementation based off of Java's Logger class     **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <logrecord.h>
#include <string>
#include <level.h>

using namespace Logging;

LogRecord::LogRecord ()
{}

LogRecord::LogRecord (std::string message)
    : message_ (message)
{}

LogRecord::LogRecord (Level level, std::string message)
    : log_level_ (level), message_ (message)
{}

LogRecord::LogRecord (std::string logger_name, std::string message)
    : logger_name_ (logger_name), message_ (message)
{}

LogRecord::LogRecord (Level level, std::string logger_name, std::string message)
    : log_level_ (level), logger_name_ (logger_name), message_ (message)
{}

LogRecord::LogRecord (Level level, std::string logger_name, std::string message,
        long time)
    : log_level_ (level), logger_name_ (logger_name), message_ (message),
    time_ (time)
{}

LogRecord::~LogRecord ()
{}

Level
LogRecord::get_level ()
{
    return this->log_level_;
}

std::string
LogRecord::get_logger_name ()
{
    return this->logger_name_;
}

std::string
LogRecord::get_message ()
{
    return this->message_;
}

long
LogRecord::get_time ()
{
    return this->time_;
}

void
LogRecord::set_level (Level l)
{
    this->log_level_ = l;
}

void
LogRecord::set_logger_name (std::string logger_name)
{
    this->logger_name_ = logger_name;
}

void
LogRecord::set_message (std::string message)
{
    this->message_ = message;
}

void
LogRecord::set_time (long time)
{
    this->time_ = time;
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
