/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_FORMATTER_H
#define LOGGING_FORMATTER_H

#include <string>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       Formatter
 * @brief       Base class provides common functionality for all formatters.
 *
 * @details     Abstract class that provides limited functionality and defines
 *              general behavior for all formatters.
 */
class Formatter
{
public:
    Formatter                               (std::string logger_name);
    virtual ~Formatter                      ();

    virtual std::string format              (LogRecord& record) = 0;
    virtual std::string format_message      (LogRecord& record) = 0;
    virtual std::string get_head            ();
    virtual std::string get_tail            ();

protected:
    /** The name of the logger the formatter is attached to */
    std::string         logger_name_;

    virtual std::string get_time_str        (time_t time);
};

}

#endif // LOGGING_FORMATTER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
