/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Formatter class for messages logged to the console/terminal **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <formatter.h>
#include <iostream>
#include <string>

using namespace Logging;


Formatter::Formatter () {}

Formatter::~Formatter () {} 

/**
 * @brief       Called once when the Logger is first opened.
 */
std::string
Formatter::get_head ()
{
    return "";
}

/**
 * @brief       Called once when the Logger is first closed.
 */
std::string
Formatter::get_tail ()
{
    return "";
}

/**
 * @brief       Formats the time of the log as a \c std::string.
 *
 * @details     Creates a string of the log time in the format HH:mm:ss
 * @param time  The time that the message was logged.
 * @return      Time of log as a string.
 */
std::string
Formatter::get_time_str (time_t time)
{
    char buf[10];
    struct tm log_time = *localtime (&time);
    strftime (buf, sizeof (buf), "%X", &log_time);
    return buf;
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
