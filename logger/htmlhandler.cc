/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <htmlhandler.h>

#include <fstream>
#include <string>

#include <iso646.h>

#include <filehandler.h>
#include <htmlformatter.h>
#include <level.h>
#include <logrecord.h>

using namespace Logging;


/**
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 */
HTMLHandler::HTMLHandler (std::string logger, std::string file_name)
    : FileHandler (new HTMLFormatter(logger), file_name)
{}

/**
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 */
HTMLHandler::HTMLHandler (std::string logger, std::ofstream *ofile)
    : FileHandler (new HTMLFormatter(logger), ofile)
{}

/**
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param file_name The name of the file that the logs are going to be 
 *                  written to
 * @param level     The maximum @c Level of logs that will be written.
 */
HTMLHandler::HTMLHandler (std::string logger, std::string file_name,
        Level level)
    : FileHandler (new HTMLFormatter(logger), file_name, level)
{}

/**
 * @brief           Construct new FileHandler
 *
 * @param logger    The name of the logger the Handler is attached to. Used
 * @param ofile     A pointer to the output file that the logs are going
 *                  to be written to.
 * @param level     The maximum @c Level of logs that will be written.
 */
HTMLHandler::HTMLHandler (std::string logger, std::ofstream *ofile,
        Level level)
    : FileHandler (new HTMLFormatter(logger), ofile, level)
{}

HTMLHandler::~HTMLHandler ()
{}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
