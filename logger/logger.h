/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @version    1.1.2                                                       **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <set>
#include <unordered_map>

#include <handler.h>
#include <level.h>

/**
 * @namespace   Logging
 * @brief       Contains classes and datatypes used for logging.
 */
namespace Logging
{

/**
 * @class       Logger
 * @brief       Logs messages to all registered handlers.
 * @details     Meta class which manages handlers such as ConsoleHandler and
 *              FileHandler and logs messages to all registered handlers.
 */
class Logger
{
public:
    /* Static functions */
    static Logger*  get_logger              (std::string name);
    static Logger*  get_anonymous_logger    ();
    /* Destructor */
    virtual ~Logger                         ();
    /* Handler related functions */
    void            add_handler             (Handler* handler);
    const std::set <Handler*> 
                    get_handlers            ();
    void            remove_handler          (Handler* handler);
    /* Level getter and setter */
    Level           get_level               ();
    void            set_level               (Level level);
    /* Get the loggers name */
    std::string     get_name                ();
    /* Logging functions */
    void            debug1                  (std::string message);
    void            debug2                  (std::string message);
    void            debug3                  (std::string message);
    void            debug4                  (std::string message);
    void            entering                (std::string c, std::string f);
    void            error                   (std::string message);
    void            exiting                 (std::string c, std::string f);
    void            info                    (std::string message);
    void            log                     (std::string message);
    void            log                     (Level level, std::string message);
    void            warning                 (std::string message);

private:
    /** Hash map that maps all existing Logger instances to their name */
    static std::unordered_map <std::string, Logger*> loggers_;
    /** Set of all Handlers registered to the Logger */
    std::set <Handler*>                     handlers_;
    /** The current logging level of the Logger */
    Level                                   log_level_;
    /** The name of the logger */
    const std::string                       name_;
    /* 
     * Restrict access to the constructor so that Logger instances are gotten
     * from get_logger or get_anonymous_logger
     */
    Logger                                  (std::string name);
};

}

#endif // LOGGER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
