/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Logging implementation based off of Java's Logger class     **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#include <logger.h>

#include <iso646.h>
#include <time.h>

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>

#include <consolehandler.h>
#include <handler.h>
#include <level.h>
#include <logrecord.h>

using namespace Logging;

std::unordered_map <std::string, Logger*> Logger::loggers_;

/* Static functions */
Logger*
Logger::get_logger (std::string name)
{
    Logger* logger;
    logger = loggers_[name];
    if (NULL == logger) {
        logger = new Logger (name);
        Logger::loggers_[name] = logger;
    }
    return logger;
}

Logger*
Logger::get_anonymous_logger ()
{
    return get_logger ("");
}

/* Constructor & Destructor */
Logger::Logger (std::string name)
    : name_ (name)
{
    Handler *default_handler = new ConsoleHandler ();
    handlers_.insert (default_handler);
    log_level_ = logINFO;
}

Logger::~Logger ()
{
    std::unordered_map <std::string, Logger*>::iterator iter;

    for (auto& h : handlers_) {
        delete h;
    }
    
    loggers_.erase (iter);
}

/* Handler related functions */
void
Logger::add_handler (Handler* handler)
{
    handlers_.insert (handler);
}

const std::set <Handler*> 
Logger::get_handlers ()
{
    return handlers_;
}

void
Logger::remove_handler (Handler* handler)
{
    auto iter = handlers_.find (handler);
    if (iter not_eq handlers_.end ())
        handlers_.erase (iter);
}

/* Level getter and setter */
Level
Logger::get_level ()
{
    return log_level_;
}

void
Logger::set_level (Level level)
{
    log_level_ = level;
}

std::string
Logger::get_name ()
{
    return name_;
}

/* Logging functions */
void
Logger::debug1 (std::string message)
{
    log (logDEBUG1, message);
}

void
Logger::debug2 (std::string message)
{
    log (logDEBUG2, message);
}

void
Logger::debug3 (std::string message)
{
    log (logDEBUG3, message);
}

void
Logger::debug4 (std::string message)
{
    log (logDEBUG4, message);
    
}

void
Logger::entering (std::string c, std::string f)
{
    log (logDEBUG1, "Entering " + c + "::" + f);
}

void
Logger::error (std::string message)
{
    log (logSEVERE, message);
}

void
Logger::exiting  (std::string c, std::string f)
{
    log (logDEBUG1, "Exiting " + c + "::" + f);
}

void
Logger::info (std::string message)
{
    log (logINFO, message);
}

void
Logger::log (std::string message)
{
    log (logINFO, message);
}

void
Logger::log (Level level, std::string message)
{
    LogRecord record (level, name_, message, time (0));
    for (Handler *h : handlers_)
        h->publish (record);
}

void
Logger::warning (std::string message)
{
    log (logWARNING, message);
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
