/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_BASE_CONSOLE_FORMATTER_H
#define LOGGING_BASE_CONSOLE_FORMATTER_H

#include <string>
#include <formatter.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       BaseConsoleFormatter
 * @brief       Base class for console based formatters.
 *
 * @details     Abstract class that provides common functionality for
 *              formatters that write to a console or terminal.
 */
class BaseConsoleFormatter : public Formatter
{
public:
    BaseConsoleFormatter                    ();
    virtual ~BaseConsoleFormatter           ();

    virtual std::string format              (LogRecord& record);
    virtual std::string format_message      (LogRecord& record);

private:
    virtual std::string get_level_str       (Level level) = 0;
};

}

#endif // LOGGING_BASE_CONSOLE_FORMATTER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
