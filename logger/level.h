/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGER_LEVEL_H
#define LOGGER_LEVEL_H

namespace Logging
{
/**
 * @enum        LogLevel
 * @brief       Enumerated types for possible logging levels.
 * @details     Possible logging levels are structured so that the most
 *              important levels are defined first/have the lowest level.
 */
enum Level {
     /** Don't log anything */
    logNONE,
    /** The log is severe; Only log errors */
    logSEVERE,
    /** The log is a warning; Log warnings and errors */
    logWARNING,
    /** The log is information; Log info, warnings, and errors */
    logINFO,
    /** 1st level debug; Log debug1 and lower */
    logDEBUG1,
    /** 2nd level debug; Log debug2 and lower */
    logDEBUG2,
    /** 3rd level debug; Log debug3 and lower */
    logDEBUG3,
    /** 4th level debug; Log everything */
    logDEBUG4
};
}

#endif // LOGGER_LEVEL_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
