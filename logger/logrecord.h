/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_LOGRECORD_H
#define LOGGING_LOGRECORD_H

#include <string>
#include <level.h>

namespace Logging
{

/**
 * @class       LogRecord
 * @brief       Stores all information about a log event.
 *
 * @details     Object passed to a Handler, then to a formatter so that
 *              formatting can be done on a log before it is written to the
 *              output stream.
 */
class LogRecord
{
public:
    LogRecord                               ();
    LogRecord                               (std::string message);
    LogRecord                               (Level level, std::string message);
    LogRecord                               (std::string logger_name,
                                             std::string message);
    LogRecord                               (Level level,
                                             std::string logger_name,
                                             std::string message);
    LogRecord                               (Level level,
                                             std::string logger_name,
                                             std::string message, long time);
    ~LogRecord                              ();

    Level           get_level               ();
    std::string     get_logger_name         ();
    std::string     get_message             ();
    long            get_time                ();
    void            set_level               (Level l);
    void            set_logger_name         (std::string logger_name);
    void            set_message             (std::string message);
    void            set_time                (long time);
private:
    Level                                   log_level_;
    std::string                             logger_name_;
    std::string                             message_;
    time_t                                  time_;
};

}

#endif // LOGGING_LOGRECORD_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
