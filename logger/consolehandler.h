/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_CONSOLE_HANDLER_H
#define LOGGING_CONSOLE_HANDLER_H

#include <formatter.h>
#include <handler.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       ConsoleHandler
 * @brief       Handler that logs messages to the console.
 */
class ConsoleHandler : public Handler
{
public:
    ConsoleHandler                          ();
    ConsoleHandler                          (Level level);
    virtual ~ConsoleHandler                 ();

    virtual void        publish             (LogRecord& record);
};

}

#endif // LOGGING_CONSOLE_HANDLER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
