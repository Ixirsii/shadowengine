/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_CONSOLE_FORMATTER_H
#define LOGGING_CONSOLE_FORMATTER_H

#include <string>

#include <baseconsoleformatter.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       ConsoleFormatter
 * @brief       Formats messages for output to a 16 color linux terminal.
 */
class ConsoleFormatter : public BaseConsoleFormatter
{
public:
    ConsoleFormatter                        ();
    virtual ~ConsoleFormatter               ();

private:
    std::string         get_level_str       (Level level);
};

}

#endif // LOGGING_CONSOLE_FORMATTER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
