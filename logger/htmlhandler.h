/** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @since      1.0.0                                                       **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/

#ifndef LOGGING_HTML_HANDLER_H
#define LOGGING_HTML_HANDLER_H

#include <fstream>
#include <string>

#include <formatter.h>
#include <filehandler.h>
#include <level.h>
#include <logrecord.h>

namespace Logging
{

/**
 * @class       HTMLHandler
 * @brief       Handler that logs messages to the console.
 */
class HTMLHandler : public FileHandler
{
public:
    HTMLHandler                             (std::string logger,
                                             std::string file_name);
    HTMLHandler                             (std::string logger,
                                             std::ofstream *ofile);
    HTMLHandler                             (std::string logger,
                                             std::string file_name,
                                             Level level);
    HTMLHandler                             (std::string logger,
                                             std::ofstream *ofile, Level level);
    virtual ~HTMLHandler                    ();

    virtual void        publish             (LogRecord& record);
};

}

#endif // LOGGING_HTML_HANDLER_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/

