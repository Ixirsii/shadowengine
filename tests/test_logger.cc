/** *********************************************************************** **
 ** @file                                                                   **
 ** @brief      Test cases for ShadowEngine/Logger                          **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @license    BSD 3-Clause                                                **
 **                                                                         **
 ** *********************************************************************** **/


#include <iostream>
#include <string>

#include <time.h>

#include <consoleformatter.h>
#include <consolehandler.h>
#include <level.h>
#include <logger.h>
#include <logrecord.h>


class CTest
{
public:    
    CTest                                  ();
    ~CTest                                 ();

    void                test_format         ();
    void                test_format_message ();
    void                test_publish        ();

private:
    Logging::ConsoleFormatter   *formatter_;
    Logging::ConsoleHandler     *handler_;
    Logging::LogRecord          *record_;
};

CTest::CTest ()
{
    std::string message = "This is a log message";
    formatter_ = new Logging::ConsoleFormatter ();
    handler_ = new Logging::ConsoleHandler ();
    record_ = new Logging::LogRecord (message);

    record_->set_level (Logging::logINFO);
    record_->set_logger_name ("Test Logger");
    record_->set_time (time (0));
}

CTest::~CTest ()
{
    delete formatter_;
    delete handler_;
    delete record_;
}

void
CTest::test_format ()
{
    // Error
    record_->set_level (Logging::logERROR);
    std::cout << formatter_->format (*record_) << std::endl;
    // Warning
    record_->set_level (Logging::logWARNING);
    std::cout << formatter_->format (*record_) << std::endl;
    // Info
    record_->set_level (Logging::logINFO);
    std::cout << formatter_->format (*record_) << std::endl;
    // Debug 1
    record_->set_level (Logging::logDEBUG1);
    std::cout << formatter_->format (*record_) << std::endl;
    // Debug 2
    record_->set_level (Logging::logDEBUG2);
    std::cout << formatter_->format (*record_) << std::endl;
    // Debug 3
    record_->set_level (Logging::logDEBUG3);
    std::cout << formatter_->format (*record_) << std::endl;
    // Debug 4
    record_->set_level (Logging::logDEBUG4);
    std::cout << formatter_->format (*record_) << std::endl;
}

void
CTest::test_format_message ()
{
    std::cout << formatter_->format_message (*record_) << std::endl;
}

void
CTest::test_publish ()
{
    handler_->publish (*record_);
}

void test_logger ()
{
    Logging::Logger *logger = Logging::Logger::get_logger ("testlogger");
    logger->error ("This is an error");
    logger->warning ("This is a warning");
    logger->info ("This is a log message");
    logger->log ("This is a log message");
    logger->debug1 ("This is an important debug message");
    logger->debug2 ("This is a less important debug message");
    logger->debug3 ("This is a debug message");
    logger->debug4 ("This is an unimportant debug message");
}
int
main (void) {
    std::cout << "Beginning the tests." << std::endl;
    CTest test;
    test.test_publish ();
    test.test_format_message ();
    test.test_format ();
    std::cout << "Testing a Logger instance" << std::endl;
    test_logger ();
    return 0;
}

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
