/*
 * Copyright © 2014 Ryan Porterfield <http://ryanporterfield.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Tests the component managing part of EntityManager system   **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @version    1.0.0:20141231                                              **
 ** @license    GNU Lesser General Public License (LGPL) v3                 **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include "../entity/entity.hpp"
#include <stdint.h>
#include <ciso646>
#include <cstdio>
#include <iostream>
#include <string>
#include <unordered_set>
#include <gtest/gtest.h>


class ComponentName : public shadowengine::entity::Component {
  public:
    static const uint32_t COMPONENT_ID = 0;

    ComponentName(std::string name) : Component(COMPONENT_ID), name_(name) {}

    virtual ~ComponentName() {}

    std::string name() const { return name_; }

    std::string set_name(std::string name) {
        std::string temp = name_;
        name_ = name;
        return temp;
    }

    friend std::ostream& operator<< (std::ostream& o,
                                     const ComponentName& name) {
        return o << name.name();
    }

  private:
    std::string name_;
};

class ComponentPosition : public shadowengine::entity::Component {
  public:
    static const uint32_t COMPONENT_ID = 1;

    ComponentPosition() : Component(COMPONENT_ID), x_(0), y_(0), z_(0) {}

    ComponentPosition(double x, double y, double z)
            : Component(COMPONENT_ID), x_(x), y_(y), z_(z) {}

    virtual ~ComponentPosition() {}

    virtual double x() const { return x_; }

    virtual double y() const { return y_; }

    virtual double z() const { return z_; }

    virtual void set_x(double x) { x_ = x; }

    virtual void set_y(double y) { y_ = y; }

    virtual void set_z(double z) { z_ = z; }

  private:
    double x_, y_, z_;
};


class ComponentManagerTest : public ::testing::Test {
  public:
    ComponentManagerTest() {}

    virtual ~ComponentManagerTest () {}

    void AddNames() {
        manager.AddComponent(uuid_0, std::make_unique<ComponentName>("Rion"));
        manager.AddComponent(uuid_1, std::make_unique<ComponentName>("Jehk"));
    }

    virtual void RemoveNames() {
        manager.RemoveComponent(uuid_0, ComponentName::COMPONENT_ID);
        manager.RemoveComponent(uuid_1, ComponentName::COMPONENT_ID);
    }

    virtual void SetUp() {
        // Create 3 new entities
        uuid_0 = manager.CreateEntity();
        uuid_1 = manager.CreateEntity();
        for (int i = 0; i < 3; ++i)
            manager.CreateEntity();
        AddNames();
    }

    virtual void TearDown() {
        std::vector<std::string> entities(*(manager.GetEntities()));
        for (std::string uuid : entities)
            manager.KillEntity(uuid);
    }

    std::string uuid_0;
    std::string uuid_1;
    shadowengine::entity::EntityManager manager;
};


TEST_F(ComponentManagerTest, AddsComponents) {
    for (std::string uuid : *(manager.GetEntities())) {
        if ((uuid == uuid_0) or (uuid == uuid_1)) {
            EXPECT_TRUE(manager.HasComponent(uuid,
                        ComponentName::COMPONENT_ID));
        } else {
            EXPECT_FALSE(manager.HasComponent(uuid,
                        ComponentName::COMPONENT_ID));
        } // end if
    }
}


TEST_F(ComponentManagerTest, RemovesComponents) {
    RemoveNames();
    for (std::string uuid : *(manager.GetEntities()))
        EXPECT_FALSE(manager.HasComponent(uuid, ComponentName::COMPONENT_ID));
}


TEST_F(ComponentManagerTest, GetComponents) {
    ComponentName* name;
    name = (ComponentName*) manager.GetComponent(uuid_0,
            ComponentName::COMPONENT_ID);
    EXPECT_EQ("Rion", name->name());
    name = (ComponentName*) manager.GetComponent(uuid_1,
            ComponentName::COMPONENT_ID);
    EXPECT_EQ("Jehk", name->name());
}

TEST_F(ComponentManagerTest, GetAllComponents) {
    manager.AddComponent(uuid_0, std::make_unique<ComponentPosition>(3, 3, 3));
    std::vector<shadowengine::entity::Component*> components;
    components = manager.GetAllComponents(uuid_0);
    EXPECT_EQ(2, components.size());
}


int main(int argc, char** argv) {
    std::cout << "Initializing Google Test Framework" << std::endl;
    ::testing::InitGoogleTest (&argc, argv);
    std::cout << "Running Tests" << std::endl;
    return RUN_ALL_TESTS ();
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
