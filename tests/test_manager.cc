/*
 * Copyright © 2014 Ryan Porterfield <http://ryanporterfield.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Tests for the core EntityManager system                     **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @version    1.0.0:20141231                                              **
 ** @license    GNU Lesser General Public License (LGPL) v3                 **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include "../entity/entity.hpp"
#include <algorithm>
#include <string>
#include <unordered_set>
#include <gtest/gtest.h>


class EntityManagerTest : public ::testing::Test {
  public:
    EntityManagerTest() {}

    virtual ~EntityManagerTest() {}

    virtual void SetUp() {
        uuid_0 = manager.CreateEntity();
        uuid_1 = manager.CreateEntity();
        uuid_2 = manager.CreateEntity();
        uuid_3 = manager.CreateEntity();
    }

    typename std::vector<std::string>::const_iterator Find(std::string uuid) {
        const std::vector<std::string>* entities = manager.GetEntities();
        return std::find(entities->begin(), entities->end(), uuid);
    }

    std::string uuid_0;
    std::string uuid_1;
    std::string uuid_2;
    std::string uuid_3;
    shadowengine::entity::EntityManager manager;
};


TEST_F(EntityManagerTest, CreatesEntities) {
    const std::vector<std::string>* entities = manager.GetEntities();
    EXPECT_EQ(4, entities->size());
}


TEST_F(EntityManagerTest, AddsEntities) {
    const std::vector<std::string>* entities = manager.GetEntities();
    EXPECT_NE(entities->end(), Find(uuid_0));
    EXPECT_NE(entities->end(), Find(uuid_1));
    EXPECT_NE(entities->end(), Find(uuid_2));
    EXPECT_NE(entities->end(), Find(uuid_3));
}


TEST_F(EntityManagerTest, IsEnabledInitially) {
    EXPECT_TRUE(manager.IsEnabled(uuid_0));
    EXPECT_TRUE(manager.IsEnabled(uuid_1));
    EXPECT_TRUE(manager.IsEnabled(uuid_2));
    EXPECT_TRUE(manager.IsEnabled(uuid_3));
}


TEST_F(EntityManagerTest, DisablesEntities) {
    manager.Disable(uuid_1);
    manager.Disable(uuid_2);
    EXPECT_TRUE(manager.IsEnabled(uuid_0));
    EXPECT_FALSE(manager.IsEnabled(uuid_1));
    EXPECT_FALSE(manager.IsEnabled(uuid_2));
    EXPECT_TRUE(manager.IsEnabled(uuid_3));
}


TEST_F(EntityManagerTest, ReEnableEntities) {
    manager.Enable(uuid_1);
    manager.Enable(uuid_2);
    EXPECT_TRUE(manager.IsEnabled(uuid_0));
    EXPECT_TRUE(manager.IsEnabled(uuid_1));
    EXPECT_TRUE(manager.IsEnabled(uuid_2));
    EXPECT_TRUE(manager.IsEnabled(uuid_3));
}


TEST_F(EntityManagerTest, KillsEntities) {
    const std::vector<std::string>* entities;
    manager.KillEntity(uuid_0);
    manager.KillEntity(uuid_2);
    entities = manager.GetEntities();
    ASSERT_EQ(2, entities->size());
    EXPECT_EQ(entities->end(),
            std::find(entities->begin(), entities->end(), uuid_0));
    EXPECT_NE(entities->end(), Find(uuid_1));
    EXPECT_EQ(entities->end(), Find(uuid_2));
    EXPECT_NE(entities->end(), Find(uuid_3));
}


int main(int argc, char** argv) {
    testing::InitGoogleTest (&argc, argv);
    return RUN_ALL_TESTS ();
}
